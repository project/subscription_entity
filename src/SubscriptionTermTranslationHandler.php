<?php

namespace Drupal\subscription_entity;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for subscription_term.
 */
class SubscriptionTermTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
