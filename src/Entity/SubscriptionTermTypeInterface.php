<?php

namespace Drupal\subscription_entity\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Subscription Term type entities.
 */
interface SubscriptionTermTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
