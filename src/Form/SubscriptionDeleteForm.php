<?php

namespace Drupal\subscription_entity\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Subscription entities.
 *
 * @ingroup subscription
 */
class SubscriptionDeleteForm extends ContentEntityDeleteForm {

}
