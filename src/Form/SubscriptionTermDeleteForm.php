<?php

namespace Drupal\subscription_entity\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Subscription Term entities.
 *
 * @ingroup subscription
 */
class SubscriptionTermDeleteForm extends ContentEntityDeleteForm {


}
